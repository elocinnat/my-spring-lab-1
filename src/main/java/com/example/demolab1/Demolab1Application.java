package com.example.demolab1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demolab1Application {

    public static void main(String[] args) {
        SpringApplication.run(Demolab1Application.class, args);
    }

}
